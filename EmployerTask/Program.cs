﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using MySql.Data.MySqlClient;
using System.IO;

namespace EmployerTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new EmployerTaskDBEntities();
            StreamWriter log;

            if (!File.Exists("logfile.txt"))
            {
                log = new StreamWriter("logfile.txt");
            }
            else
            {
                log = File.AppendText("logfile.txt");
            }
            Console.WriteLine("Press any key to connect to database");
            Console.ReadKey();
            
            Console.WriteLine("type in the pathname of the csv file to read");
            string csvPath = Console.ReadLine();
            try
            {
                using (TextFieldParser parser = new TextFieldParser(csvPath))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    parser.TrimWhiteSpace = true;
                    bool firstRow = true;
                    int count = 0;
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string[] fields = parser.ReadFields();
                        if (firstRow)
                        {
                            firstRow = false;
                            continue;
                        }
                        FeedRow.NewRow(fields);

                        //But here's another assumption:
                        // if we see a UPC code that we have already processed we skip
                        var upcEntryExists = context.itemdetails.Where(x => x.UPC == FeedRow.UPC).FirstOrDefault();
                        if (upcEntryExists != null)
                        {
                            log.WriteLine(DateTime.Now);
                            log.WriteLine("UPC code :"+FeedRow.UPC+ " has already been processed. Duplicate");
                            log.WriteLine();
                            continue;
                        }

                        //Add to Item table if new SKU. Note that if item is null then we run down process
                        // of adding entries into seasons, category, and color table if they do not exist already
                        // and we also add references to those in the itemSeason and itemCategory tables.
                        // since we assume season and categories are dependant on Model/SKU alone we don't have 
                        // to worry about running through the add everything logic every time we have a new item
                        // with some model/SKU we've already processed. 
                        // Note that we still add entry for the upc which carries color and size etc. with it.
                        var item = context.items.Where(x => x.SKU == FeedRow.SKU).FirstOrDefault();
                        if (item == null)
                        {
                            item = new item()
                            {
                                SKU = FeedRow.SKU,
                                Name = FeedRow.Name,
                                Tax = FeedRow.Tax,
                                Price = FeedRow.Price,
                                Description = FeedRow.Description
                            };
                            context.items.Add(item);
                            context.SaveChanges();
                            //we only add categories once per item since categories (we assume) is bound to sku
                            foreach (string categoryName in FeedRow.Categories)
                            {
                                if (!String.IsNullOrWhiteSpace(categoryName))
                                {
                                    var category = context.categories.Where(x => x.Name == categoryName).FirstOrDefault();
                                    if (category == null)
                                    {
                                        category = new category()
                                        {
                                            Name = categoryName
                                        };
                                        context.categories.Add(category);
                                        context.SaveChanges();
                                    }
                                    var itemCategory = new itemcategory()
                                    {
                                        ItemId = item.ItemId,
                                        CategoryId = category.CategoryId
                                    };
                                    context.itemcategories.Add(itemCategory);
                                    context.SaveChanges();
                                }
                            }
                            // create season entry if none exists and then create reference to this item using itemSeason Table
                            var season = context.seasons.Where(x => x.Name == FeedRow.Season).FirstOrDefault();
                            if (season == null)
                            {
                                season = new season()
                                {
                                    Name = FeedRow.Season,
                                    Code = FeedRow.SeasonCode
                                };
                                context.seasons.Add(season);
                                context.SaveChanges();
                            }
                            var itemSeason = new itemseason()
                            {
                                ItemId = item.ItemId,
                                SeasonId = season.SeasonId
                            };
                            context.itemseasons.Add(itemSeason);
                            context.SaveChanges();
                        }

                        //now we add entry for Item detail, regardless if SKU has been processed, since UPC is unique

                        //create clolor entry if none exists, and do the same with size
                        var color = context.colors.Where(x => x.Name == FeedRow.Color).FirstOrDefault();
                        if (color == null)
                        {
                            color = new color()
                            {
                                Name = FeedRow.Color,
                                Code = FeedRow.ColorCode
                            };
                            context.colors.Add(color);
                            context.SaveChanges();
                        }
                        var size = context.sizes.Where(x => x.Name == FeedRow.Size).FirstOrDefault();
                        if (size == null)
                        {
                            size = new size()
                            {
                                Name = FeedRow.Size
                            };
                            context.sizes.Add(size);
                            context.SaveChanges();
                        }
                        //Regardless of condition above, add to ItemDetail table
                        itemdetail itemDetail = new itemdetail()
                        {
                            UPC = FeedRow.UPC,
                            ItemId = item.ItemId,
                            ColorId = color.ColorId,
                            SizeId = size.SizeId,
                            OnHand = FeedRow.OnHand,
                            OnSale = FeedRow.OnSale,
                            SalePrice = FeedRow.SalePrice
                        };
                        context.itemdetails.Add(itemDetail);
                        context.SaveChanges();
                        count++;
                    }
                    Console.WriteLine("There were " + count + " number of entries.");
                    Console.WriteLine("Press any key to exit.");
                    Console.ReadKey();
                }
            }
            catch(System.IO.FileNotFoundException ex)
            {
                log.WriteLine(DateTime.Now);
                log.WriteLine(ex.Message);
                log.WriteLine();
            }
            catch (Exception ex)
            {
                log.WriteLine(DateTime.Now);
                log.WriteLine(ex.Message);
                log.WriteLine();
            }
            finally
            {
                log.Close();
            }
            
            //input pathname to read from file
        }
        public static class FeedRow
        {
            public static String UPC { get; set; }
            public static String Name { get; set; }
            public static String SKU { get; set; }
            public static bool Tax { get; set; }
            public static decimal Price { get; set; }
            public static String Color { get; set; }
            public static String Size { get; set; }
            public static int OnHand { get; set; }
            public static String Season { get; set; }
            public static string[] Categories { get; set; }
            public static String ColorCode { get; set; }
            public static String SeasonCode { get; set; }
            public static String Description { get; set; }
            public static decimal? SalePrice { get; set; }
            public static bool OnSale { get; set; }

            static FeedRow() {
            }
            public static void NewRow(string[] fields)
            {
                UPC = fields[0];
                Name = fields[1];
                SKU = fields[2];
                Tax = fields[3] == "Y"? true: false;
                Price = Convert.ToDecimal( fields[4]);
                Color = fields[5];
                Size = fields[6];
                OnHand = Convert.ToInt32(fields[7]);
                Season = fields[8];
                ColorCode = fields[12];
                SeasonCode = fields[13];
                Description = fields[14];
                SalePrice = String.IsNullOrWhiteSpace(fields[15])? null : (decimal?) Convert.ToDecimal(fields[15]);
                OnSale = fields[16] == "Y" ? true : false;
                Categories = new string[] { fields[9], fields[10], fields[11] };
                
            }

        }
    }
}
